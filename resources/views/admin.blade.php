@extends('layouts.app') 
@section('content')


<div class="container">
  <div class="container">
    <div class="row d-flex align-items-center justify-content-center ">
      <div class="col-xs-10 col-sm-10 col-md-10 col-lg-7 showtop-content text-center">
        <h1>Detail Staff</h1>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="table-responsive">
      <table class="table table-striped ">
        <thead class="thead-dark">
          <tr>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Phone Number</th>
            <th scope="col">Type</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          @foreach($detailstaff as $next)
          <tr>

            <td>{{$next->fname}}</td>
            <td>{{$next->lname}}</td>
            <td>{{$next->phone}}</td>
            <td>{{$next->user_type}}</td>
            <td>
             
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalchangetype{{$next->id}}"><i class="fas fa-address-book"></i></button>
             
            </td>
          </tr>

          <form method="POST" action="{{ url('deletepost', $id = $next->id) }}">
            @csrf @method('DELETE')
            <div class="modal fade" id="modalchangetype{{ $next->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
              aria-hidden="true">
              <div class="modal-dialog modal-sm modal-notify modal-danger" role="document">
                <div class="modal-content text-center">
                  <div class="modal-header d-flex justify-content-center">
                    <p class="heading">Are you sure?</p>
                  </div>
                  <div class="modal-body">
                    <select class="custom-select">
                      <option selected>Open this select menu</option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                    </select>
                  </div>
                  <div class="modal-footer flex-center">
                    <button type="submit" class="btn  btn-outline-danger"> Yes </button>
                    <a href="" class="btn  btn-outline-danger" data-dismiss="modal">No</a>
                  </div>
                </div>

              </div>
            </div>
          </form>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
      <div class="d-flex justify-content-center">
        {{ $detailstaff->links() }}
      </div>
    </div>
  </div>
</div>
@endsection