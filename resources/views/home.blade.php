@extends('layouts.app') 
@section('content')
<section id="showtop">
  <div class="container">
    <div class="row d-flex align-items-center justify-content-center ">

      <div class="col-xs-10 col-sm-10 col-md-10 col-lg-7 showtop-content text-center">
        <h1>Welcome to <span class="showtop-span">Laravel Basic</span></h1>
        <p>A Laravel basic built with HTML5, Css and Bootstrap</p>
      </div>
    </div>

  </div>
</section>


<section id="showicon">
  <div class="container">
    <div class="row ">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
        <h2>Laravel Basic</h2>
        <p>What's Included</p>

        <div class="row ">
          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <i class="fas fa-exclamation-triangle" id="showicon-i"></i></i><br>
            <h4>Laravel Authentication</h4>
            <p> Laravel makes implementing authentication very simple. In fact, almost everything is configured for you out of
              the box. The authentication configuration file is located at config/auth.php, which contains several well documented
              options for tweaking the behavior of the authentication services.</p>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <i class="fas fa-lock" id="showicon-i"></i><br>
            <h4>Laravel Middleware</h4>
            <p> Middleware provide a convenient mechanism for filtering HTTP requests entering your application. For example,
              Laravel includes a middleware that verifies the user of your application is authenticated. If the user is not
              authenticated, the middleware will redirect the user to the login screen. However, if the user is authenticated,
              the middleware will allow the request to proceed further into the application.</p>
          </div>

        </div>


        <div class="row ">
          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <i class="fas fa-database" id="showicon-i"></i><br>
            <h4>Laravel Database: Migrations </h4>
            <p> Migrations are like version control for your database, allowing your team to easily modify and share the application's
              database schema. Migrations are typically paired with Laravel's schema builder to easily build your application's
              database schema. If you have ever had to tell a teammate to manually add a column to their local database schema,
              you've faced the problem that database migrations solve.</p>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <i class="fab fa-css3" id="showicon-i"></i><br>

            <h4>Bootstrap</h4>
            <p> Build responsive, mobile-first projects on the web with the world’s most popular front-end component library.
              Bootstrap is an open source toolkit for developing with HTML, CSS, and JS. Quickly prototype your ideas or
              build your entire app with our Sass variables and mixins, responsive grid system, extensive prebuilt components,
              and powerful plugins built on jQuery.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="showphilosophy">
  <div class="container">
    <div class="row ">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 d-flex justify-content-center align-items-center ">

        <img id="imlogo" class="" src="{{URL::to('/')}}/image/weblogo.png" alt="Responsive image" />
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
        <h2 class="bt-4">Laravel Philosophy</h2>
        <p>
          Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative
          experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks
          used in the majority of web projects, such as authentication, routing, sessions, and caching. Laravel aims to make
          the development process a pleasing one for the developer without sacrificing application functionality. Happy developers
          make the best code. To this end, we've attempted to combine the very best of what we have seen in other web frameworks,
          including frameworks implemented in other languages, such as Ruby on Rails, ASP.NET MVC, and Sinatra. Laravel is
          accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of
          control container, expressive migration system, and tightly integrated unit testing support give you the tools
          you need to build any application with which you are tasked.</p>

      </div>
    </div>
  </div>
</section>





<section id="showabout">
  <div class="container text-center">
    <div class="row">
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <h4 class="text-center">Contact Us</h4><br>
        <i class="fab fa-laravel"> <a href="https://laravel.com/"> Laravel Website Official</a></i>



      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <h4 class="text-center">About Us</h4><br>
        <p>Developer </p>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
        <h4 class="text-center">Newsletter</h4><br>
        <p></p>

      </div>
    </div>
  </div>
</section>
@endsection