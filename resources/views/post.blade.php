@extends('layouts.app') 
@section('content')

<div class="container">
    <div class="row  justify-content-center">
        <div class="card col-xs-12 col-sm-12 col-md-12 col-lg-8  mt-4">
            <div class="card-body ">
                <form class="" method="POST" action="{{ url('staffstore') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <h2 class="card-title">POST </h2>
                        <hr> @if (count($errors)>0)
                        <div class="alert alert-danger ">
                            <ul>@foreach ($errors->all() as $errors)
                                <li>{{$errors}}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif @if (\Session::has('success'))
                        <div class="alert alert-success ">
                            <p>{{\Session::get('success')}}</p>
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput2">Title</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput2">Detail Post</label>
                        <textarea type="text" class="form-control" name="detail" id="detail">
                        </textarea>
                    </div>


                    <div class="form-group">
                        <label for="exampleFormControlFile1">Image</label>
                        <input type="file" id="file" name="file" class="form-control-file" id="exampleFormControlFile1">
                    </div>


                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection