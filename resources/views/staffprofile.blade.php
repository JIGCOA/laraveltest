@extends('layouts.app') 
@section('content')
<div class="container mt-4 mb-4">
    @foreach($detailprofile as $next)
    <div class="card ">
        <div class="card-body text-center ">
            <p class="h4 mb-4">Profile</p>
            <div class="form-group ">
                <div class="row">
                    <label for="" class="col-md-4">First Name</label>
                    <input type="text" name="" id="" class="form-control col-md-6 " placeholder="{{$next->fname}} " disabled>
                </div>
            </div>
            <div class="form-group">
                <div class="row ">
                    <label for="" class="col-md-4">Last Name</label>
                    <input type="text" name="" id="" class="form-control col-md-6 " placeholder="{{$next->lname}}" disabled>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                @if (count($errors)>0)
                <div class="alert alert-danger col-md-6">
                    <ul>@foreach ($errors->all() as $errors)
                        <li>{{$errors}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif @if (\Session::has('success'))
                <div class="alert alert-success col-md-6">
                    <p>{{\Session::get('success')}}</p>
                </div>
                @endif
            </div>
            <div class="form-group">
                <div class="row ">
                    <label for="" class="col-md-4">Phone Number</label>
                    <input type="text" name="" id="" class="form-control col-md-6" placeholder="{{$next->phone}}" disabled>
                </div>
            </div>
            <div class="form-group">
                <div class="row ">
                    <div class="col-md-4"></div>
                    <button class="btn btn-primary col-md-6   " data-toggle="modal" data-target="#exampleModal{{$next->id}}">Edit</button>
                </div>
            </div>
        </div>
    </div>
</div>


<form method="POST" action="{{ url('/updetas'), $id = $next->id }}">
    @csrf @method('DELETE')
    <input name="_method" type="hidden" value="PATCH">
    <div class="modal fade" id="exampleModal{{$next->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Phone Number</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4">Phone Number</label>
                            <input type="text" name="phone" id="phone" class="form-control col-md-6" value="{{$next->phone}}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save </button>
                </div>
            </div>
        </div>
    </div>
</form>
@endforeach
@endsection