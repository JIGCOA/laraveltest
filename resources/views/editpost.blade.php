@extends('layouts.app') 
@section('content')

<div class="container">
    <div class="row d-flex align-items-center justify-content-center ">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 showtop-content text-center">
            <h1>POST</h1>

        </div>
    </div>

    <div class="row">
        @if (count($errors)>0)
        <div class="alert alert-danger col-md-12">
            <ul>@foreach ($errors->all() as $errors)
                <li>{{$errors}}</li>
                @endforeach
            </ul>
        </div>
        @endif @if (\Session::has('success'))
        <div class="alert alert-success col-md-12">
            <p>{{\Session::get('success')}}</p>
        </div>
        @endif

        <div class="table-responsive">
            <table class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th class="">Title</th>
                        <th>Detail</th>
                        <th>Time</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($post as $next)
                    <tr>

                        <td> {{ $next->title}}</td>
                        <td> {{substr($next->detail, 0, 40)}}</td>
                        <td> {{ $next->created_at}}</td>
                        <td>

                            <div class="row">
                                <div class="m-1">

                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalConfirmContent{{$next->id}}"> <i class="fas fa-eye"></i></button>
                                </div>
                                <div class="m-1">

                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalContactForm{{$next->id}}"> <i class="fas fa-edit"></i></button>
                                </div>
                                <div class="m-1">
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalConfirmDelete{{$next->id}}"><i class="fas fa-eraser"></i></button>
                                </div>

                            </div>




                            <!-- โมเดล โพส-->

                            <form method="POST" action="{{ url('deletepost', $id = $next->id) }}">
                                @csrf @method('DELETE')
                                <div class="modal fade" id="modalConfirmContent{{$next->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog modal-lm modal-notify modal-danger" role="document">
                                        <!--Content-->
                                        <div class="modal-content">
                                            <!--Header-->
                                            <div class="modal-header">
                                                <p class="heading lead">{{$next->title}}</p>

                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true" class="white-text">&times;</span>
                                            </button>
                                            </div>

                                            <!--Body-->
                                            <div class="modal-body">
                                                @if($next->image_p != NULL)
                                                <div class="text-center">
                                                    <img src="{{URL::to('/')}}/image/{{$next->image_p}}" class="img-fluid" alt="Responsive image">


                                                    <p>{{$next->detail}}</p>
                                                </div>

                                                @else
                                                <div class="text-center">


                                                    <p>{{$next->detail}}</p>
                                                </div>
                                                @endif

                                            </div>

                                            <!--Footer-->
                                            <div class="modal-footer justify-content-center">
                                                <a type="button" class="btn btn-primary" data-dismiss="modal">ปิด</a>
                                            </div>
                                        </div>
                                        <!--/.Content-->

                                    </div>
                                </div>
                            </form>
                            <!--ปิด โมเดล โพส-->
                            <!-- โมเดล ลบ-->

                            <form method="POST" action="{{ url('deletepost', $id = $next->id) }}">
                                @csrf @method('DELETE')
                                <div class="modal fade" id="modalConfirmDelete{{ $next->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog modal-sm modal-notify modal-danger" role="document">
                                        <div class="modal-content text-center">
                                            <div class="modal-header d-flex justify-content-center">
                                                <p class="heading">Are you sure?</p>
                                            </div>
                                            <div class="modal-body">
                                                <i class="fas fa-eraser fa-4x animated rotateIn"></i>
                                            </div>
                                            <div class="modal-footer flex-center">
                                                <button type="submit" class="btn  btn-outline-danger"> Yes </button>
                                                <a href="" class="btn  btn-outline-danger" data-dismiss="modal">No</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                            <!--ปิด โมเดล ลบ-->



                            <!-- โมเดล แก้ไข-->
                            <div class="modal fade" id="modalContactForm{{$next->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header text-center">
                                            <h4 class="modal-title w-100 font-weight-bold">Edit</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body mx-3">
                                            <form class="" method="POST" action="{{ url('editpost', $id = $next->id) }}" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                <input name="_method" type="hidden" value="PATCH">

                                                <div class="form-group">
                                                    <label for="formGroupExampleInput2">Title</label>
                                                    <input type="text" class="form-control" id="title" name="title" placeholder="{{$next->title}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="formGroupExampleInput2">Detail Post</label>
                                                    <textarea type="text" class="form-control" name="detail" id="detail" placeholder="{{$next->detail}}">
                                            </textarea>
                                                </div>


                                                <div class="form-group">
                                                    <label for="exampleFormControlFile1">Image</label>
                                                    <input type="file" id="file" name="file" class="form-control-file" id="exampleFormControlFile1">
                                                </div>


                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary">Edit</button>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!--ปิด โมเดล แก้ไข-->



                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
@endsection