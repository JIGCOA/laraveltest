<?php

namespace App;

use App\Profile;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id', 'fname', 'lname', 'phone', 'email', 'password', 'user_type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return mixed
     */
    public function profile()
    {

        return $this->hasOne(Profile::class, 'user_id');
    }

    public function is_user()
    {
        $is_user = Auth::user()->user_type;
        if ($is_user == "admin") {

            return redirect('/home');
        }
    }

}
