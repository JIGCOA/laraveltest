<?php

namespace App;

use App\Post;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id', 'status', 'category',
    ];

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(Usar::class);
    }
    /**
     * @return mixed
     */
    public function post()
    {
        return $this->hasMany(Post::class, 'id_user_post');
    }
}
