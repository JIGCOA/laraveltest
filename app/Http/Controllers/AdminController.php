<?php

namespace App\Http\Controllers;

use App\User;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $detailstaff = User::where('user_type', '!=', 'admin')->paginate(10);

        if ($detailstaff) {
            return view('admin',
                [
                    'detailstaff' => $detailstaff,
                ]);

        }

    }

}
