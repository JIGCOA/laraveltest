<?php

namespace App\Http\Controllers;

use App\Post;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StaffController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('staff');
    }

    public function index()
    {
        $detailprofile = User::find(Auth::user());
        if ($detailprofile) {
            return view('staffprofile',
                [
                    'detailprofile' => $detailprofile,
                ]);
        }
    }
    /**
     * @param Request $request
     */
    public function updates(Request $request)
    {
        $this->validate($request, [
            'phone' => 'required|min:10',
        ]);
        $phone = $request['phone'];
        $user = User::find(Auth::user()
                ->id);
            if ($user) {
            $user->phone = $phone;
            $user->save();

            return redirect('staff')->with('success', 'Success Update');
        }

    }
    public function post()
    {
        $user = User::find(Auth::user()
                ->id);

            return view('post', ['user' => $user]);

    }
    /**
     * @param Request $request
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:150',
            'detail' => 'required|string',
            'file' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);

        if ($request->file == null) {
            $user = User::find(Auth::user()
                    ->id);
                if ($user) {
                $newpost = new Post;
                $newpost->id_user_post = $user->profile->user_id;
                $newpost->title = $request->input('title');
                $newpost->detail = $request->input('detail');
                $newpost->image_p = "";
                $newpost->image_z = "";
                $newpost->save();

                return redirect('staffpost')->with('success', 'Successfully created post!');

            }
        } else {
            $user = User::find(Auth::user()
                    ->id);
                if ($user) {
                $newpost = new Post;
                $newpost->id_user_post = $user->profile->user_id;
                $newpost->title = $request->input('title');
                $newpost->detail = $request->input('detail');

                $image = $request->file('file');
                $path = public_path('/image');
                $image_name = time() . '.' . $image->getClientOriginalName();
                $newpost->image_p = $image_name;
                $newpost->image_z = $image->getClientSize();
                $newpost->save();
                if ($newpost->save()) {
                    $image->move($path, $image_name);

                }

                return redirect('staffpost')->with('success', 'Successfully created post!');

            }
        }
    }
    public function showpost()
    {
        //
        // $blog = Blog::findOrFail($id);
        if ($id = Auth::user()->id) {
            $post = Post::all()->where('id_user_post', '=', $id);

            // show the view and pass the blog to it

            return view('editpost',
                [
                    'post' => $post,
                ]);

        }
        Auth::logout();

        return Redirect::route('/register');

    }
    /**
     * @param $id
     */
    public function deletes($id)
    {
        $post = Post::findOrFail($id);

        if ($post->image_p != null) {
            $image_path = public_path('/image') . '/' . $post->image_p;
            if (file_exists($image_path)) {
                unlink($image_path);
            }
            $post->delete();

        }
        $post->delete();

        return redirect('staffshowpost')
            ->with('success', 'Product deleted successfully');
    }
    /**
     * @param Request $request
     * @param $id
     */
    public function editpost(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|string|max:150',
            'detail' => 'required|string',
            'file' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);
        if ($request->file == null) {
            $editpost = Post::findOrFail($id);
            if ($editpost) {

                $editpost->title = $request->input('title');
                $editpost->detail = $request->input('detail');
                $editpost->save();

                return redirect('staffshowpost')->with('success', 'Successfully created post!');

            }
        } else {
            $editpost = Post::findOrFail($id);
            if ($editpost) {
                $editpost->title = $request->input('title');
                $editpost->detail = $request->input('detail');
                $image_path = public_path('/image') . '/' . $editpost->image_p;
                if (file_exists($image_path)) {
                    unlink($image_path);
                }

                $image = $request->file('file');
                $path = public_path('/image');
                $image_name = time() . '.' . $image->getClientOriginalName();
                $editpost->image_p = $image_name;
                $editpost->image_z = $image->getClientSize();
                $editpost->save();
                if ($editpost->save()) {
                    $image->move($path, $image_name);

                }

                return redirect('staffshowpost')->with('success', 'Successfully created post!');

            }
        }
    }
    /**
     * @param $id
     */
    public function changetype($id)
    {

    }

}
