<?php

namespace App;

use App\Profile;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    /**
     * @var array
     */
    protected $fillable = [
        'id_user_post', 'title', 'detail', 'image_p', 'image_z',
    ];
    /**
     * @return mixed
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class, 'id');
    }
}
