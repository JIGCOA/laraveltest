<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('/home');
});
Route::get('/newhome', function () {
    return view('/NewHome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'AdminController@index');

Route::resource('post', 'PostController', ['only' => [
    'index', 'store', 'show',
]]);

Route::resource('post', 'PostController', ['except' => [
    'create', 'update', 'destroy',
]]);
Route::patch('/updetas', 'StaffController@updates');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index');
});

Route::get('/staff', 'StaffController@index')
    ->middleware('staff')
    ->name('staff');

Route::get('/staffpost', 'StaffController@post')
    ->middleware('staff')
    ->name('staffpost');

Route::post('/staffstore', 'StaffController@store')
    ->middleware('staff')
    ->name('staffstore');

Route::get('/staffshowpost', 'StaffController@showpost')
    ->middleware('staff')
    ->name('staffshowpost');

Route::delete('/deletepost/{id}', 'StaffController@deletes')
    ->middleware('staff')
    ->name('deletepost');

Route::patch('/editpost/{id}', 'StaffController@editpost')
    ->middleware('staff')
    ->name('editpost');
