<?php

use Illuminate\Database\Seeder;

class AddadminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
            'fname' => 'Super',
            'lname' => 'Admin',
            'phone' => '0000000000',
            'email' => 'admin@admin.com',
            'password'=> Hash::make('123456'),
            'user_type' => 'admin',
            'created_at' => date('Y-m-d H:i:s')
            ]
            
        ]);
        DB::table('profiles')->insert([
            [
            'user_id' => '1',
            'status' => 'on',
            'created_at' => date('Y-m-d H:i:s')
            ]
            
        ]);
    }
}
